package com.zuitt.com;

public interface Greetings {
    public void morningGreet();
    public void holidayGreet();
}
