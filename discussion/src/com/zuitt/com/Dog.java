package com.zuitt.com;


// Child Class of Animal
// "extend" keyword is used to inherit the properties and methods of the parent class.
public class Dog extends Animal {
    // properties
    private String breed;

    // constructor
    public Dog(){
        super();
        this.breed = "Chihuahua";
    }

    public Dog(String name, String color, String breed){
        super(name, color);
        this.breed = breed;
    }
    // Setter and Getter
    public String getBreed(){
        return this.breed;
    }
    public void setBreed(String breed){
        this.breed = breed;
    }

    // method
    public void speak(){
        System.out.println("Woof woof!");
    }


}

