package com.zuitt.com;

public class Person implements Actions, Greetings {
    public void sleep() {
        System.out.println("Zzzzzzz....");
    }

    public void run() {
        System.out.println("Running.");
    }

    public void morningGreet(){
        System.out.println("Good morning!");
    }
    public void holidayGreet() {
        System.out.println("Happy Holidays!");
    }

}
